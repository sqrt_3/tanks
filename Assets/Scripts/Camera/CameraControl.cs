﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float m_DampTime = 0.2f;                 
    public float m_ScreenEdgeBuffer = 4f;           
    public float m_MinSize = 6.5f;                  
    [HideInInspector] public Transform[] m_Targets; 


    private Camera m_Camera;                        
    private float m_ZoomSpeed;                      
    private Vector3 m_MoveVelocity;                 
    private Vector3 m_DesiredPosition;              


    private void Awake()
    {
        //Store camera child object into m_Camera variable
        m_Camera = GetComponentInChildren<Camera>();
    }


    private void FixedUpdate()
    {
        //Move and zoom child camera obj accordingly
        Move();
        Zoom();
    }


    private void Move()
    {
        //Find average position to put the camera in
        FindAveragePosition();

        //Smoothly damp (i.e move to) that position that the camera is supposed to be
        transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }


    private void FindAveragePosition()
    {
        //Create an empty vector to store the average position.
        //Loop through all target objects, but consider only those that are active (i.e alive tanks)
        //Add each tank's position into the average position vector, and increment the total number of targets (tanks) considered
        //If there was 1 or more tank to account for, then calculate the average position by dividing the vector by the number of targets
        //Set the average Y position to current Y position to prevent the camera from moving along the Y axis (since tanks can't move along the Y axis)
        //Store the average into m_DesiredPosition variable
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        for (int i = 0; i < m_Targets.Length; i++)
        {
            if (!m_Targets[i].gameObject.activeSelf)
                continue;

            averagePos += m_Targets[i].position;
            numTargets++;
        }

        if (numTargets > 0)
            averagePos /= numTargets;

        averagePos.y = transform.position.y;

        m_DesiredPosition = averagePos;
    }


    private void Zoom()
    {
        //Calculate the required size for the camera, and smoothly damp (i.e scale/adjust) the camera's size to match it and allow for all tanks to be seen on screen
        float requiredSize = FindRequiredSize();
        m_Camera.orthographicSize = Mathf.SmoothDamp(m_Camera.orthographicSize, requiredSize, ref m_ZoomSpeed, m_DampTime);
    }


    private float FindRequiredSize()
    {
        //Camera's orthographic size accounts for what the dimensions are for the stuff ingame to be displayed (i.e what the player will see)
        //Camera's aspect is divinding the aspect ratio of the screen (i.e 16/9 = 1.778 for 1920x1080 res)
        //Height from center is size, length from the center to an edge is size * aspect (from camera rig's local axis)
        //Locally, the size = distance in y axis from center to top. If tank is at a larger distance than size, we need to zoom out.
        //Locally, the distance in x axis from the center to right = size * aspect. Can use this equation to get size = distance x axis / aspect. (We can tweak camera size to account for it)
        //Create a vector from the DESIRED position, not the current position since we want to find the size from that position we wish to move to.
        //Create and initialize a size variable that will be responsible for holding the size that the camera is supposed to be to fit all tanks on screen
        //Loop through targets (tanks), but only account for active (alive) ones

        //For each tank: 
        //      create a vector targetLocalPos that will give us the target's position locally
        //      we've found the tank's local position (targetLocalPos) and the camera's desired local position (desiredLocalPos)
        //      so, subtract the tank's local space from the desired position for the camera and store it in a vector that represents the position from the desired camera pos to the target's pos
        //      compare the actual value of size to the absolute value of the desired camera's y position, and store the largest value into size
        //      compare the actual value of size (which is now the largest number of the desired camera's y position or the value of size) to the absolute value of the result of dividing the distance on the x axis by the camera's aspect (calculation we derived above for size) and store the largest out of the two into size

        //once the loop is over, the largest value is now stored in size.
        //we need to add the screen edge buffer to make sure that the tanks have some "breathing room" with respect to the camera's edges
        //we compare that value of size with the minimum camera size (this is done to prevent the camera from zooming in way too deep) and store the largest value into size, and return it

        Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        float size = 0f;

        for (int i = 0; i < m_Targets.Length; i++)
        {
            if (!m_Targets[i].gameObject.activeSelf)
                continue;

            Vector3 targetLocalPos = transform.InverseTransformPoint(m_Targets[i].position);

            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.y));

            size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / m_Camera.aspect);
        }
        
        size += m_ScreenEdgeBuffer;

        size = Mathf.Max(size, m_MinSize);

        return size;
    }


    public void SetStartPositionAndSize()
    {
        //We find the average position for the start of the game (to be used when resetting/creating a new game)
        //We set the camera's position to the desired position (the one that allows for all tanks to be seen)
        //We set the camera's size to the required size (the one that allows for all tanks to be seen with some "breathing room" on edges)
        FindAveragePosition();

        transform.position = m_DesiredPosition;

        m_Camera.orthographicSize = FindRequiredSize();
    }
}