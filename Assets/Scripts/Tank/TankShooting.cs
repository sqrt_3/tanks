﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;       
    public Rigidbody m_Shell;
    public Rigidbody m_MachineGun;
    public Transform m_FireTransform;    
    public Slider m_AimSlider;           
    public AudioSource m_ShootingAudio;  
    public AudioClip m_ChargingClip;     
    public AudioClip m_FireClip;
    public AudioClip m_EmptyClip;
    public AudioClip m_MachineGunClip;
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f;
    public int m_StartingShellAmmo = 15;
    public float m_ShootingInterval = 1.0f;
    public float m_MachineGunIntensity = 15.0f;

    private string m_FireButton;
    private string m_SecondaryFireButton;        
    private float m_CurrentLaunchForce;  
    private float m_ChargeSpeed;         
    private bool m_Fired;
    private int m_CurrentShellAmmo;
    private bool m_CanFire;
    private float m_Timer;

    private void OnEnable()
    {
        //When turned back on, set min slider & launch force
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
        m_Timer = 0.0f;
    }


    private void Start()
    {
        //Called once at start, calculating fire button and charging speed
        m_FireButton = "Fire" + m_PlayerNumber;
        m_SecondaryFireButton = "SubFire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

        m_CurrentShellAmmo = m_StartingShellAmmo;
        m_Timer = 0.0f;
    }

    private void Update()
    {
        //Update timer, if <= 0.0f then tank can fire
        m_Timer -= Time.deltaTime;
        if (m_Timer <= 0.0f)
            m_CanFire = true;
        else
            m_CanFire = false;

        if (m_CanFire) //should only compute firing code if player can fire
        {
            // Track the current state of the fire button and make decisions based on the current launch force.
            m_AimSlider.value = m_MinLaunchForce;

            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                //at max charge, not fired
                m_CurrentLaunchForce = m_MaxLaunchForce;
                Fire(true);
            }
            else if (Input.GetButtonDown(m_FireButton))
            {
                //pressed fire for first time, obviously not firing
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;

                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play();
            }
            else if (Input.GetButton(m_FireButton) && !m_Fired)
            {
                //holding button, not fired
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
                m_AimSlider.value = m_CurrentLaunchForce;
            }
            else if (Input.GetButtonUp(m_FireButton) && !m_Fired)
            {
                //released button, not fired yet
                Fire(true);
            }
        }

        if(Input.GetButtonUp(m_SecondaryFireButton))
        {
            //released secondary shooting button - this is a machine gun powered shot
            Fire(false);
        }
    }


    private void Fire(bool isMissile)
    {
        //Shell-related shooting code
        if(isMissile)
        {
            // Instantiate and launch the shell.
            m_Fired = true;

            if (m_CurrentShellAmmo > 0)
            {
                Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
                shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
                m_ShootingAudio.clip = m_FireClip;
                m_ShootingAudio.Play();
                --m_CurrentShellAmmo;
            }
            else
            {
                m_ShootingAudio.clip = m_EmptyClip;
                m_ShootingAudio.Play();
            }


            m_CurrentLaunchForce = m_MinLaunchForce;
            m_Timer = m_ShootingInterval;
        }

        //Machine gun related code
        else
        {
            Rigidbody mgInstance = Instantiate(m_MachineGun, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
            mgInstance.velocity = m_MachineGunIntensity * m_FireTransform.forward;
            m_ShootingAudio.clip = m_MachineGunClip;
            m_ShootingAudio.Play();
            
        }
    }

    public void SetAmmo(int amount)
    {
        m_CurrentShellAmmo = amount;
    }

    public void GiveAmmo(int amount)
    {
        m_CurrentShellAmmo += amount;
    }
}