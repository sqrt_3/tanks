﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        Pickup pickup = other.GetComponent<Pickup>();
        if(pickup)
        {
            switch(pickup.m_PickupType)
            {
                case Pickup.ePickupType.Health:
                    TankHealth health = GetComponentInParent<TankHealth>();
                    health.GiveHealth(pickup.m_increasingFactor);
                    health.SetHealthUI();
                    break;
                case Pickup.ePickupType.Ammo:
                    TankShooting shooting = GetComponentInParent<TankShooting>();
                    shooting.GiveAmmo((int)pickup.m_increasingFactor);
                    break;
            }
        }
        Destroy(other.gameObject);
    }
}
