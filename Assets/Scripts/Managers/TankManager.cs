﻿using System;
using UnityEngine;

[Serializable] //can show instance of this class in the inspector
public class TankManager
{
    public Color m_PlayerColor;            
    
    public Transform[] m_SpawnPoints;
    [HideInInspector] public int m_PlayerNumber;             
    [HideInInspector] public string m_ColoredPlayerText;
    [HideInInspector] public GameObject m_Instance;          
    [HideInInspector] public int m_Wins;
    [HideInInspector] public Transform m_SpawnPoint;

    private TankMovement m_Movement;       
    private TankShooting m_Shooting;
    private GameObject m_CanvasGameObject;


    //called by game manager when first creating tanks
    //find references to movement/shooting as well as canvas, set player number on scripts, generate html color string
    //make array of mesh renderers in children that are mesh renderers, and set their material's color to players color
    public void Setup()
    {
        m_Movement = m_Instance.GetComponent<TankMovement>();
        m_Shooting = m_Instance.GetComponent<TankShooting>();
        m_CanvasGameObject = m_Instance.GetComponentInChildren<Canvas>().gameObject;

        m_Movement.m_PlayerNumber = m_PlayerNumber;
        m_Shooting.m_PlayerNumber = m_PlayerNumber;

        m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER " + m_PlayerNumber + "</color>";

        MeshRenderer[] renderers = m_Instance.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = m_PlayerColor;
        }

        //respawn randomly when first set up
        int random = UnityEngine.Random.Range(0, m_SpawnPoints.Length);
        m_SpawnPoint = m_SpawnPoints[random];
        
    }

       
    //called by game manager, turns off scripts (move/shoot) and canvas
    public void DisableControl()
    {
        m_Movement.enabled = false;
        m_Shooting.enabled = false;

        m_CanvasGameObject.SetActive(false);
    }

    //called by game manager, turns on scripts (move/shoot) and canvas
    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Shooting.enabled = true;

        m_CanvasGameObject.SetActive(true);
    }

    //sets instance back to spawn point, turns it off, then turns it on again (all tanks will be off except for winner - need to turn everyone off before turning on)
    public void Reset()
    {
        //respawn randomly when reset
        int random = UnityEngine.Random.Range(0, m_SpawnPoints.Length);
        m_SpawnPoint = m_SpawnPoints[random];

        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Shooting.SetAmmo(m_Shooting.m_StartingShellAmmo);

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }
}
