﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public enum ePickupType
    {
        Health,
        Ammo
    };

    public ePickupType m_PickupType;
    public float m_increasingFactor = 10.0f;
    
}
