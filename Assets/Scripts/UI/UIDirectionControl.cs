using UnityEngine;

public class UIDirectionControl : MonoBehaviour
{
    public bool m_UseRelativeRotation = true;  


    private Quaternion m_RelativeRotation;     


    private void Start()
    {
        //intialize relative rotation to parent's local rotation
        m_RelativeRotation = transform.parent.localRotation;
    }


    private void Update()
    {
        //ensure UI rotation continues to stay at the same place if relative rotation is enabled (which it is in this case)
        if (m_UseRelativeRotation)
            transform.rotation = m_RelativeRotation;
    }
}
