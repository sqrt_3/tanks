#Tanks - a simple modification of a Unity Game Tutorial

##What's new?
* Random spawns have been set in place - each player has 5 designated spawn possible choices, one of them chosen randomly every round
* Shell capacity is now limited - players start with 15 (currently) shells 
* There's a cooldown between firing shells - approximately 1 second. This, together with the limited capacity, forces the player to use shells in a smart way, not just spamming them.
* A new weapon has been added to the game: the machine gun. It has unlimited ammo, and there's no cooling time between shots, but it requires greater accuracy and does less damage than the shells.
* Pickups have been added to the game: There's 2 pickup types, health and armor, and they each give a respective amount of health/ammo to the player that gets it.


####Credits
* Unity - software
* Unite 2015 (Boston) - convention where the tutorial took place
* Will Goldstone and James Bouckley - trainers of basic "Tanks" version
* Matias Lago - improvements listed above

